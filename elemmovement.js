class ElemMovement {

    DIRECTION_UP = "upwards";
    DIRECTION_DOWN = "downwards";

    EFFECT_SCALE = "scale";
    EFFECT_BLUR = "blur";
    EFFECT_MOVE_DOWN = "movedown";
    EFFECT_MOVE_UP = "moveup";
    EFFECT_BACKGROUNDSIZE = "backgroundsize";

    /**
     * Initializes the element movement script for the provided element.
     * 
     * @param {JSON} params A JSON object with a key-value pair where the key is the name 
     * of the parameter and the value is the value to be assigned to it.
     * 
     * The params object can be filled in with the following parameters:
     * 
     * <b>moving_element:</b> the element that has to be moved depending on the scrolling position on 
     * the page.
     * 
     * <b>direction:</b> The direction in which the element has to move. This accepts 'horizontal' or 'vertical' as values.
     * 
     * <b>start_offset:</b> The page offset at which the element has to start being moved.
     * 
     * <b>end_offset:</b> The distance after which the element has to stop moving.
     * 
     * <b>effect:</b> A JSON array where the key is the name of the effect (which are listed below) and the value is 
     * another JSON array with the desired minimum and maximum values. The value can also be 'null' if
     * you want to use the defaults.
     * <ul>
     * <li>scale: Scales the element up when scrolling</li>
     * <li>blur: Applies a blur filter on the element.</li>
     * <li>movedown: Moves the element down in a parrallax way.</li>
     * <li>moveup: Moves the element up in a parrallax way.</li>
     * </ul>
     * Additionally, to add minimum and maximum values, add the following:
     * <ul>
     * <li>min: the minimum value (as an integer)</li>
     * <li>max: the maximum value (as an integer)</li>
     * </ul>
     * 
     * An example:
     * 
     * let mvmt = new ElemMovement({
     *  ...
     *  effect: {
     *        blur: {
     *              min: 20,
     *              max: 50
     *        },
     *        backgroundsize: null 
     *     }
     * });
     * 
     * The above example will apply a blur effect with as lowest value '20px' and highest value '50px'. On top of this, 
     * it will also resize the background using the default values.
     */
    constructor(params) {
        this.movingElement = params.moving_element;
        this.direction = params.direction;
        this.startOffset = params.start_offset;
        this.endOffset = (params.end_offset == undefined) ? Infinity : params.end_offset;
        this.effect = (params.effect == undefined) ? ["blur"] : params.effect;

        this._lastScrollTopOffset = window.scrollY;
        this.body = document.querySelector('body');

        window.addEventListener('scroll', (event) => this._scrollEventHappened(event));
    };

    _scrollEventHappened = (event) => {
        let direction = this._determineScrollDirection();

        if (direction == this.DIRECTION_DOWN) {
            this._increaseEffect();
        } else if (direction == this.DIRECTION_UP) {
            this._decreaseEffect();
        }
        this._lastScrollTopOffset = window.scrollY;
    }

    _determineScrollDirection = () => {
        let direction;
        if (this._lastScrollTopOffset < window.scrollY) {
            direction = this.DIRECTION_DOWN;
        } else {
            direction = this.DIRECTION_UP;
        }
        return direction;
    }

    _increaseEffect = () => {
        if (this._lastScrollTopOffset >= this.startOffset && !this._isEndOffsetReached()) {
            if (this.effect.blur) {
                this._applyBlur();
            }

            if (this.effect.backgroundsize) {
                this._applyBackgroundResize();
            }

            if (this.effect.movedown) {
                this._increaseMoveDown();
            }

            if (this.effect.moveup) {
                this._increaseMoveUp();
            }

            if (this.effect.scale) {
                this._increaseScale();
            }
        }
    }

    _decreaseEffect = () => {
        if (this._lastScrollTopOffset >= this.startOffset && !this._isStartOffsetReached()) {
            if (this.effect.blur) {
                this._applyBlur();
            }

            if (this.effect.backgroundsize) {
                this._applyBackgroundResize();
            }

            if (this.effect.movedown) {
                this._decreaseMoveDown();
            }

            if (this.effect.moveup) {
                this._decreaseMoveUp();
            }

            if (this.effect.scale) {
                this._decreaseScale();
            }
        }
    }

    _applyBlur = () => {
        let blurMin = (this.effect.blur.min) ? this.effect.blur.min : 0;
        let blurMax = (this.effect.blur.max) ? this.effect.blur.max : 50;

        let styles = {
            filter: "blur(" + this._getRelativeValue(blurMin, blurMax) + "px)"
        };

        this._applyStyles(styles);
    }

    _applyBackgroundResize = () => {
        let sizeMin = (this.effect.backgroundsize.min) ? this.effect.backgroundsize.min : 100;
        let sizeMax = (this.effect.backgroundsize.max) ? this.effect.backgroundsize.max : 200;

        let styles;
        
        if(this._isEndOffsetReached()){
            styles = {
                backgroundSize: this._getRelativeValue(sizeMin, sizeMax) + "%"
            }
        } else if(this._isStartOffsetReached()) {
            styles = {
                backgroundSize: sizeMin + "%"
            }
        } else {
            styles = {
                backgroundSize: this._getRelativeValue(sizeMin, sizeMax) + 100 + "%"
            };
        }
        

        this._applyStyles(styles);
    }

    /**
     * Gets a value out of a minimum and a maximum number, relative to the 
     * amount that has been scrolled in between the start and end offsets. 
     */
    _getRelativeValue = (min = 0, max = 50) => {
        if (this._isStartOffsetReached()) {
            return min;
        } else if (this._isEndOffsetReached()) {
            return max;
        } else {
            let scrollPct = this._getScrolledPct();
            let distance = this._getRelativeDistance(min, max);

            return (distance / 100) * scrollPct;
        }
    }

    /**
     * Applies the given style to the movingElement.
     */
    _applyStyles = (styles) => {
        for (let prop in styles) {
            this.movingElement.style[prop] = styles[prop];
        }
    }

    _isEndOffsetReached = () => {
        return (window.scrollY >= this.endOffset);
    }

    _isStartOffsetReached = () => {
        return (window.scrollY <= this.startOffset);
    }

    /**
     * Gets the relative distance between two values. 
     * This is calculated by substracting the start value from the 
     * end value.
     */
    _getRelativeDistance = (start, end) => {
        return end - start;
    }

    /**
     * Gets the total scroll distance between the start offset and the end offset.
     * If the start offset is 0 and the end offset is 200, the relative scroll distance is 200.
     * If the start offset is 300 and the end offset is 600, then the relative scroll distance is 300.
     * In short, the relative scroll distance is calculated by substracting the start offset 
     * from the end offset.
     */
    _getRelativeScrollDistance = () => {
        return this.endOffset - this.startOffset;
    }

    /**
     * Gets the distance scrolled relative to the relative scroll distance. 
     * So, if the start offset is 200 and the end offset is 500, the relative scroll distance is 300.
     * If the user has scrolled until an offset of 250, the relative distance scrolled will be of 50.
     * In short, the relative distance scrolled is calculated by 
     * substracting the start offset from the current scroll postion of the user.
     * 
     */
    _getRelativeScrolled = () => {
        return window.scrollY - this.startOffset;
    }

    _getScrolledPct = () => {
        return (this._getRelativeScrolled() / this._getRelativeScrollDistance()) * 100;
    }
}